#ifndef __DYNAMICOBJECT_H__
#define __DYNAMICOBJECT_H__

#include "Vector3.h"
#include "GameObject.h"
class DynamicObject :
	public GameObject
{
protected:
	Vector3 _speed;
public:
	DynamicObject(double x, double y, double z, double sx, double sy , bool direction);
	~DynamicObject();
	virtual void update(double delta_t) = 0;
	void setSpeed(const Vector3 &speed);
	void setSpeed(double x, double y, double z);
	Vector3 getSpeed();
	bool isCollidible();
};

#endif 

