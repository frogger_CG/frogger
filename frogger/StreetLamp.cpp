#include "StreetLamp.h"


StreetLamp::StreetLamp(double x, double y, double z) : StaticObject(x, y, z)
{
}


StreetLamp::~StreetLamp()
{
}

void StreetLamp::draw(){

	GLfloat amb[] = { 0.05f, 0.05f, 0.05f, 1.0f };
	GLfloat diff[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	GLfloat spec[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat shine = 10.f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);

	glPushMatrix();
	glTranslatef(getPosX(), getPosY(), getPosZ()+ 2.5);
	glScalef(0.5, 0.5, 5);
	glutSolidCube(1); 
	glPopMatrix();

	glPushMatrix();
	glTranslatef(getPosX() , getPosY(), getPosZ() + 5);
	glScalef(1, 0.5, 0.5);
	glutSolidSphere(1, 10 ,10);
	glPopMatrix();
}