#ifndef __ORTHOGONALCAMERA_H__
#define __ORTHOGONALCAMERA_H__

#include "Camera.h"

class OrthogonalCamera :
	public Camera
{
private:
	double _left, _right, _top, _bottom;
public:
	OrthogonalCamera(double left, double right, double top, double bottom, double near, double far): Camera(near, far){
		_left = left;
		_right = right;
		_top = top;
		_bottom = bottom;
	}
	void update(int width, int height);
	void computeProjectionMatrix();
	void computeVisualizationMatrix();
	~OrthogonalCamera();
};

#endif

