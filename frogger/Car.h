#ifndef __CAR_H__
#define __CAR_H__

#include "DynamicObject.h"
class Car :
	public DynamicObject
{
public:
	Car(double x, double y, double z, double speed, bool dir);
	void draw();
	void drawWheel();
	void drawSquare();
	void setDirection(int dir);
	void stop();
	void update(double dt);
	bool isMovingSet();
	~Car();
};
#endif

