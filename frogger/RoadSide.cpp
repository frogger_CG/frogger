#include "RoadSide.h"
#include <GL\glut.h>


RoadSide::RoadSide(double x, double y, double z) : StaticObject(x,y,z)
{
	_name = "roadside.jpg";
}


RoadSide::~RoadSide()
{
}

void RoadSide::draw(){


	/* RoadSide Cores*/
	GLfloat amb[] = { 0.08f, 0.07f, 0.09f, 1.0f };
	GLfloat diff[] = { 0.34f, 0.33f, 0.43f, 1.0f };
	GLfloat spec[] = { 0.13f, 0.09f, 0.13f, 1.0f };
	GLfloat shine = 83.0f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);
	glPushMatrix();
	glColor3f(0.5f, 0.5f, 0.5f);
	glEnable(GL_TEXTURE_2D);
	LoadTexture();
	desenhaMalha(-15, 15, -8,-6.9 , 0.5);
	desenhaMalha(-15, 15, -0.8, 0.4, 0.5);
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
};