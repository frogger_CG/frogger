#ifndef __CAMERA_H__
#define __CAMERA_H__


#define UP 1
#define DOWN 2
#define LEFT 3
#define RIGHT 4


#include "Entity.h"
class Camera :
	public Entity
{
protected:
	Vector3 _up;
	Vector3 _at;
	double _near;
	double _far;
	float _camPosX = 0.0, _camPosY = -10.0, _camX = 0 , _camY = - 2;
	int _direction;
public:
	Camera(double near, double far): Entity(0,0,0) {
		_near = near;
		_far = far;
	}
	~Camera();
	virtual void update(int width, int height) = 0;
	virtual void computeProjectionMatrix() = 0;
	virtual void computeVisualizationMatrix() = 0;
	void setPositionCamera(float camPosX, float camPosY);
	void setDirection(int dir);
	void setDirectionVar(int dir);
};

#endif

