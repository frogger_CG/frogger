#include "Riverside.h"
#include <GL/glut.h>

Riverside::Riverside(double x, double y, double z) : StaticObject(x, y, z)
{
	_name = "riverside.png";
}

void Riverside::draw(){

	
	/*
	glTranslatef(getPosX(), getPosY(), getPosZ());
	glColor3f(0.0f, 0.9f, 0.0f);
	glScalef(30, 1.5, 1);
	glutSolidCube(1);
	*/
	/* comment the material*/
	GLfloat amb[] = { 0.05f, 0.2f, 0.05f, 1.0f };
	GLfloat diff[] = { 0.0f, 0.56f, 0.0f, 1.0f };
	GLfloat spec[] = { 0.39f, 0.54f, 0.41f, 1.0f };
	GLfloat shine = 76.8f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);
	
	glPushMatrix();
	glColor3f(0.0f, 0.9f, 0.0f);
	glEnable(GL_TEXTURE_2D);
	LoadTexture();
	desenhaMalha(-15, 15, 6.2, 8, 0.5);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}


