#include "Perspective1.h"
#include "GL\glut.h"

Perspective1::~Perspective1()
{
}

void Perspective1::update(int w, int h){
	float ratio = _aspect;
	float aspect = _fovy;
	if (aspect > ratio)
		glViewport((w - h*ratio) / 2, 0, h*ratio, h);
	else
		glViewport(0, (h - w / ratio) / 2, w, w / ratio);
}

void Perspective1::computeProjectionMatrix(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(_fovy, _aspect, 1, 100);
}

void Perspective1::computeVisualizationMatrix(){
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, -10.0, 10.0, //Camera Position
		0.0, 0.0, 0.0,		//Looking at
		0.0, 1.0, 1.0);		//Up Vector
}