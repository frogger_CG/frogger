#ifndef __FROG_H__
#define __FROG_H__

#define UP 1
#define DOWN 2
#define LEFT 3
#define RIGHT 4

#include "DynamicObject.h"
class Frog :
	public DynamicObject
{
private:
	bool _up, _down, _left, _right, _canWalk;
	Vector3 _timberSpeed;
public:
	Frog(double x, double y, double z);
	~Frog(){}
	void draw();
	void setDirection(int dir);
	void setWalk(bool b);
	void setTimberSpeed(Vector3 speed);
	void stop();
	void update(double dt);
	bool isMovingSet();
	bool edgeOfBoard();
private:
	void frogbody();
};

#endif


