#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__

#ifndef SHOW_RIVER
#define SHOW_RIVER true
#endif

#ifndef SHOW_RIVERSIDE
#define SHOW_RIVERSIDE true
#endif

#include <stdlib.h>
#include "GL\glut.h"
#include <GL/SOIL.h>
#include "Entity.h" 
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <vector>


class GameObject :
	public Entity
{
private:
	Vector3 _size;
	bool _direction;
protected:
	GLuint _texture =0;
	const char* _name;
public:
	GameObject(double x, double y, double z) : Entity(x, y, z){
		}
	GameObject(double x, double y, double z, double sx, double sy, bool direction) : Entity(x,y,z){
		_size = Vector3(sx, sy, 0);
		_direction = direction;
	}
	~GameObject(){}
	virtual void draw() = 0;
	virtual void update(double delta_t) = 0;
	virtual bool isCollidible() = 0;
	Vector3 getSize();
	void restartMove();
	static void setNewX(int t);
	void setDirection(bool direction);
	bool getDirection();
	void LoadTexture();
};

#endif

