#include "GameManager.h"
#include "collisionDetetor.h"


GameManager::GameManager() : _time(glutGet(GLUT_ELAPSED_TIME)), _initTime(_time)
{
	this->init();
	
}

GameManager::~GameManager()
{
}

void GameManager::init(){
	addGameObject(new Riverside(0, 7.25, 0));
	addGameObject(new River(0,3.59,0));
	addGameObject(new RoadSide(0, -0.05, 0));
	addGameObject(new Road(0, -3.7, 0));
	addGameObject(new RoadSide(0, -7.3, 0));

	addGameObject(new Car(-13, -5.5, 0, 2,true));
	addGameObject(new Bus(-9, -5.5, 0, 2, true));
	addGameObject(new Car(-3.5, -5.5, 0, 2, true));
	addGameObject(new Car(1, -5.5, 0, 2, true));
	addGameObject(new Bus(6, -5.5, 0, 2, true));
	addGameObject(new Car(11, -5.5, 0, 2, true));

	addGameObject(new Car(17, -3.7, 0, -3, false));
	addGameObject(new Bus(-12, -3.7, 0, -3, false));
	addGameObject(new Car(-7, -3.7, 0, -3, false));
	addGameObject(new Bus(-2, -3.7, 0, -3, false));
	addGameObject(new Car(3, -3.7, 0, -3, false));
	addGameObject(new Car(8, -3.7, 0, -3, false));

	addGameObject(new Bus(-15, -2, 0, 2.5, true));
	addGameObject(new Bus(-10, -2, 0, 2.5, true));
	addGameObject(new Car(-5, -2, 0, 2.5, true));
	addGameObject(new Car(0, -2, 0, 2.5, true));
	addGameObject(new Bus(5, -2, 0, 2.5, true));
	addGameObject(new Car(10, -2, 0, 2.5, true));

	addGameObject(new TimberLog(-8, 1.5, 0, -2, false));
	addGameObject(new TimberLog(0, 1.5, 0, -2, false));
	addGameObject(new TimberLog(8, 1.5, 0, -2, false));

	addGameObject(new TimberLog(-6, 2.8, 0, 2.5, true));
	addGameObject(new TimberLog(4, 2.8, 0, 2.5, true));
	addGameObject(new TimberLog(10, 2.8, 0, 2.5, true));

	addGameObject(new TimberLog(13, 4.2, 0, -3, false));
	addGameObject(new TimberLog(3, 4.2, 0, -3, false));
	addGameObject(new TimberLog(-5, 4.2, 0, -3, false));

	addGameObject(new TimberLog(-15, 5.6, 0, 2.8, true));
	addGameObject(new TimberLog(-4, 5.6, 0, 2.8, true));
	addGameObject(new TimberLog(7, 5.6, 0, 2.8, true));
	addGameObject(new StreetLamp(-7, -7, 0));
	addGameObject(new StreetLamp(7, -7, 0));
	addGameObject(new StreetLamp(-7, 0, 0));
	addGameObject(new StreetLamp(7, 0, 0));
	addGameObject(new StreetLamp(-7, 7, 0));
	addGameObject(new StreetLamp(7, 7, 0));
	addGameObjectLife(new Frog(-14, -7.43, 5));
	addGameObjectLife(new Frog(-13, -7.43, 5));
	addGameObjectLife(new Frog(-12, -7.43, 5));
	addGameObjectLife(new Frog(-11, -7.43, 5));
	addGameObjectLife(new Frog(-10, -7.43, 5));
	addGameObjectMenu(new Menu(0, 0, 0, "gameover.png"));
	addGameObjectMenu(new Menu(0, 0, 0, "pause.png"));
	_camera = new OrthogonalCamera(-15, 15, 10, -10, -2, 2);
	_frog = new Frog(0, -7.43, 0);
	addGameObject(_frog);

	LightSource* light = new LightSource(GL_LIGHT0);
	light->setPosition(Vector4(0, 4, 5, 0));
	light->setAmbient(Vector4(0.5, 0.5, 0.5, 1.0));
	light->setDiffuse(Vector4(1.0, 1.0, 1.0, 1.0));
	light->setSpecular(Vector4(1.0, 1.0, 1.0, 1.0));
	_light_sources.push_back(light);

	int num = 1;
	int vectorx[] = { -7, 7 };
	int vectory[] = { -7, 0, 7 };

	for (int i = 0; i < 2; i++){
		for (int j = 0; j < 3; j++){
			light = new LightSource(GL_LIGHT0 + num);
			light->setPosition(Vector4(vectorx[i], vectory[j], 5.5 , 1.0));
			if (vectory[j] == -7){
				light->setDirection(Vector3(0., 0.4, -1.));
			}
			if (vectory[j] == 7){
				light->setDirection(Vector3(0., -0.4, -1.));
			}


			light->setExponent(5.0);
			light->setCutOff(60.0);

			light->setAmbient(Vector4(.8, .8, .8, 1.0));
			light->setDiffuse(Vector4(.6, .6, .6, 1.0));
			light->setSpecular(Vector4(.3, .3, .3, 1.0));

			_light_sources.push_back(light);
			num++;
		}
	}
	light = new LightSource(GL_LIGHT0 + 7);
	light->setPosition(Vector4(0, -7.43, 3, 1.0));
	light->setDirection(Vector3(0., 1, -1.));
	light->setExponent(10.0);
	light->setCutOff(45.0);
	light->setAmbient(Vector4(.8, .8, .8, 1.0));
	light->setDiffuse(Vector4(.6, .6, .6, 1.0));
	light->setSpecular(Vector4(.3, .3, .3, 1.0));
	_light_sources.push_back(light);

	_collisionDetetor = new collisionDetetor(this);

}


void GameManager::reshape(GLsizei width, GLsizei height){
	glPushMatrix();
	_width = width;
	_height = height;
	_camera->update(width, height);
	glPopMatrix();
}

void GameManager::display(void) {
	glPushMatrix();
	_camera->computeProjectionMatrix();
	_camera->computeVisualizationMatrix();

	for (int i = 0; i < 8; i++){
		_light_sources[i]->draw();
	}

	

	for (std::vector<GameObject *>::iterator it = _game_objects.begin(); it != _game_objects.end(); it++) {
		(*it)->draw();
	}
	glPopMatrix();
	

	// Camara para aparecerem as varias vidas representadas no ecra
	glPushMatrix();
	_camera1->update(_width,_height);
	_camera1->computeProjectionMatrix();
	_camera1->computeVisualizationMatrix();

	bool light;
	if (glIsEnabled(GL_LIGHTING))
		light = true;
	else{
		light = false;
	}
	glDisable(GL_LIGHTING);
	for (int j = 0; j < _lifes; j++){
		_game_objects_lifes[j]->draw();
	}

	
	if (_lifes == 0){
		// Mudanca para perspectiva ortho e visualizacao do menu game over
		_game_objects_menu[0]->draw();
	}

	if (_pause == true){
		// Mudanca para perspectiva ortho e visualizacao do menu pausa
		_game_objects_menu[1]->draw();
	}
	if (light == true)
		glEnable(GL_LIGHTING);


	glPopMatrix();
}


void GameManager::onTimer(){
	long unsigned int newTime = glutGet(GLUT_ELAPSED_TIME);
	long unsigned int dt =  newTime - _time;
	_time = newTime;

	
	if (_pause == false && _lifes != 0){
		_collisionDetetor->detectColisions();
		for (unsigned int i = 0; i < _game_objects.size(); i++) {
			_game_objects[i]->update(dt);
		}
		if (_time - _initTime > 10000 && _timerOut < 7){
			updateGameSpeed();
			_initTime = _time;
			_timerOut++;
		}
		_camera->setPositionCamera(_frog->getPosX(), _frog->getPosY());
		_light_sources[7]->setPosition(Vector4(_frog->getPosX(), _frog->getPosY(), 2, 1.0));
	}
	glutPostRedisplay();
}

void GameManager::updateGameSpeed(){
	DynamicObject* obj;
	for (unsigned int i = 0; i < _game_objects.size() - 1; i++) {
		if (_game_objects[i]->isCollidible()){
			obj = dynamic_cast<DynamicObject*>(_game_objects[i]);
			obj->setSpeed(obj->getSpeed() * 1.1);
		}
	}
}

void GameManager::keyPressed(unsigned char c, int i, int j){
	_keyPressed = c;
	if (c == 'o' || c == 'O'){
		if (_pause == false && _lifes != 0){
			_frog->setDirection(LEFT);
			_light_sources[7]->setDirection(Vector3(-1, 0, -1));
				/*Opcao para a camara virar com o sapo 
					if (camera3)
						_camera->setDirectionVar(LEFT);*/
		}
	}
	else if (c == 'q'){
		if (_pause == false && _lifes != 0){
			_frog->setDirection(UP);
			_light_sources[7]->setDirection(Vector3(0, 1, -1));
			/*if (camera3)
				_camera->setDirectionVar(UP);*/
		}
	}
	else if (c == 'p'){
		if (_pause == false && _lifes != 0){
			_frog->setDirection(RIGHT);
			_light_sources[7]->setDirection(Vector3(1, 0, -1));
			/*if (camera3)
				_camera->setDirectionVar(RIGHT);*/
		}
	}
	else if (c == 'a'){
		if (_pause == false && _lifes != 0){
			_frog->setDirection(DOWN);
			_light_sources[7]->setDirection(Vector3(0, -1, -1));
			/*if (camera3)
				_camera->setDirectionVar(DOWN);*/
		}
	}
	else if (c == '1'){
		camera3 = false;
		_camera = new OrthogonalCamera(-15, 15, 10, -10, -2, 2);
		_camera->update(_width, _height);
		glutPostRedisplay();
		
	}
	else if (c == '2'){
		camera3 = false;
		_camera = new Perspective1(120, _width / _height, 0, 50);
		_camera->update(_width, _height);
		glutPostRedisplay();
		
	}
	else if (c == '3'){
		camera3 = true;
		_camera = new Perspective2(90, _width / _height, 0, 50);
		_camera->update(_width, _height);
		glutPostRedisplay();
		
	}
	else if (c == 'l') {
		if (glIsEnabled(GL_LIGHTING)){
			glDisable(GL_LIGHTING);
		}
		else {
			glEnable(GL_LIGHTING);
			
		}
	}
	else if (c == 'n'){
		if (glIsEnabled(GL_LIGHT0)){
			glDisable(GL_LIGHT0);
			if (_headlight == true)
				glEnable(GL_LIGHT0 + 7);
		}
		else{
			glEnable(GL_LIGHT0);
			if (_headlight == true)
				glDisable(GL_LIGHT0 + 7);
		}
	}

	else if (c == 'c'){
		if (glIsEnabled(GL_LIGHT1)){
			for (int i = 1; i < 7; i++){
				glDisable(GL_LIGHT0 + i);
			}
		}
		else{
			for (int i = 1; i < 7; i++){
				glEnable(GL_LIGHT0 + i);
			}
		}
	}
	else if (c == 'h'){
		if (glIsEnabled(GL_LIGHT0 + 7)){
			glDisable(GL_LIGHT0 + 7);
			_headlight = false;
		}
		else{
			glEnable(GL_LIGHT0 + 7);
			_headlight = true;
		}
	}
	else if (c == 's'){
		_pause = !_pause;
	}
	else if (c == 'r'){
		_lifes = 5;
		_game_objects.clear();
		_light_sources.clear();
		init();
	}
}

void GameManager::keyboardUp(unsigned char c, int i, int j){
	if ((c == 'o' && _keyPressed == c) || 
		(c == 'q' && _keyPressed == c) ||
		(c == 'p' && _keyPressed == c) || 
		(c == 'a' && _keyPressed == c))
		_frog->stop();
}

void GameManager::addGameObject(GameObject * go){
	_game_objects.push_back(go);
}

void GameManager::addGameObjectLife(GameObject * go){
	_game_objects_lifes.push_back(go);
}

void GameManager::addGameObjectMenu(GameObject * go){
	_game_objects_menu.push_back(go);
}

std::vector<GameObject *> GameManager::getGameObjects(){
	return _game_objects;
}
Frog * GameManager::getFrog(){
	return _frog;
}

void GameManager::looselife(){
	--_lifes;
}

