#ifndef __TIMBERLOG_H__
#define __TIMBERLOG_H__

#include "DynamicObject.h"
class TimberLog :
	public DynamicObject
{
public:
	TimberLog(double x, double y, double z, double speed, bool dir);
	void draw();
	void setDirection(int dir);
	void stop();
	void update(double dt);
	bool isMovingSet();
	~TimberLog();
};
#endif

