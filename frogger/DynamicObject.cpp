#include "DynamicObject.h"


DynamicObject::DynamicObject(double x, double y, double z, double sx, double sy, bool direction) : GameObject(x, y, z, sx, sy, direction)
{
	_speed = Vector3(0,0,0);
	setDirection(direction);
}


DynamicObject::~DynamicObject()
{
}

void DynamicObject::setSpeed(const Vector3 &speed){
	_speed.set(speed.getX(), speed.getY(), speed.getZ());
}

void DynamicObject::setSpeed(double x, double y, double z){
	_speed.set(x, y, z);
}

Vector3 DynamicObject::getSpeed(){
	return _speed;
}

bool DynamicObject::isCollidible(){
	return true;
}