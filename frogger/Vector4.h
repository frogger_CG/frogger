#ifndef __VECTOR4_H__
#define __VECTOR4_H__

#include "vector3.h"
class Vector4 :
	public Vector3
{
protected:
	double _w;

public:
	Vector4(double x, double y, double z, double w) : Vector3(x, y, z), _w(w) {}
	~Vector4(void);
	double getW() const;
	void set(double x, double y, double z, double w);
	Vector4 operator+ (const Vector4 &vec);
	Vector4 operator- (const Vector4& vec);
	Vector4 operator* (double num);
};

#endif