#ifndef __STATICOBJECT_H__
#define __STATICOBJECT_H__

#include "GameObject.h"
class StaticObject :
	public GameObject
{
public:
	StaticObject(double x, double y, double z);
	~StaticObject();
	void update(double delta_t){}
	bool isCollidible();
	void desenhaMalha(double x1, double x2, double y1, double y2, double z1);
};

#endif 

