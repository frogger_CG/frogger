#include "StaticObject.h"


StaticObject::StaticObject(double x, double y, double z) : GameObject(x, y, z)
{
}

void StaticObject::desenhaMalha(double x1, double x2, double y1, double y2, double z1){

	for (double i = x1; i<x2; i = i + 0.5){
		for (double j = y1; j<y2; j = j + 0.5){
			glBegin(GL_TRIANGLES);   //triangulo da esquerda
			glNormal3f(0, 0, 1);
			glTexCoord2f(0, 1);
			glVertex3f(i, j + 0.5, z1);
			glTexCoord2f(0.5, 0.5);
			glVertex3f(i + 0.25, j + 0.25, z1);
			glTexCoord2f(0, 0);
			glVertex3f(i, j, z1);
			glEnd();

			glBegin(GL_TRIANGLES);   //triangulo de cima
			glNormal3f(0, 0, 1);
			glTexCoord2f(0, 1);
			glVertex3f(i, j + 0.5, z1);
			glTexCoord2f(0.5, 0.5);
			glVertex3f(i + 0.25, j + 0.25, z1);
			glTexCoord2f(1,1);
			glVertex3f(i + 0.5, j + 0.5, z1);
			glEnd();

			glBegin(GL_TRIANGLES);   //triangulo da direita
			glNormal3f(0, 0, 1);
			glTexCoord2f(1, 1);
			glVertex3f(i + 0.5, j + 0.5, z1);
			glTexCoord2f(0.5, 0.5);
			glVertex3f(i + 0.25, j + 0.25, z1);
			glTexCoord2f(1, 0);
			glVertex3f(i + 0.5, j, z1);
			glEnd();

			glBegin(GL_TRIANGLES);   //triangulo de baixo
			glNormal3f(0, 0, 1);
			glTexCoord2f(1, 0);
			glVertex3f(i + 0.5, j, z1);
			glTexCoord2f(0.5, 0.5);
			glVertex3f(i + 0.25, j + 0.25, z1);
			glTexCoord2f(0, 0);
			glVertex3f(i, j, z1);
			glEnd();
		}
	}
}




StaticObject::~StaticObject()
{
}

bool StaticObject::isCollidible(){
	return false;
}
