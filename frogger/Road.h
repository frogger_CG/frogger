#ifndef __ROAD_H__
#define __ROAD_H__
#include "StaticObject.h"
class Road :
	public StaticObject
{
public:
	Road(double x, double y, double z);
	~Road();
	void draw();
};
#endif

