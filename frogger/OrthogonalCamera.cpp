#include "OrthogonalCamera.h"
#include <GL\glut.h>




OrthogonalCamera::~OrthogonalCamera()
{
}

void OrthogonalCamera::update(int w, int h){

	float ratio = (_right - _left) / (_top - _bottom);
	float aspect = (float)w / h;
	if (aspect > ratio)
		glViewport((w - h*ratio) / 2, 0, h*ratio, h);
	else
		glViewport(0, (h - w / ratio) / 2, w, w / ratio);
}
void OrthogonalCamera::computeProjectionMatrix(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
}
void OrthogonalCamera::computeVisualizationMatrix(){
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glOrtho(_left, _right, _bottom, _top, -10, 10);
}