#ifndef __MENU_H__
#define __MENU_H__

#include "StaticObject.h"
class Menu :
	public StaticObject
{
public:
	Menu(double x, double y, double z, const char* menuName);
	~Menu();
	void draw();
};

#endif