#ifndef __ENTITY_H__
#define __ENTITY_H__

#include "Vector3.h"

class Entity
{
protected:
	Vector3 * _vector3;
public:
	Entity(double x, double y, double z){
		_vector3 = new Vector3(x,y,z);
	}
	~Entity();
	Vector3 * getPosition();
	double getPosX();
	double getPosY();
	double getPosZ();
	Vector3 * setPosition(double x, double y, double z);
	Vector3 * setPosition(const Vector3 &vec);
};

#endif
