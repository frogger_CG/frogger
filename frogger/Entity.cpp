#include "Entity.h"

Vector3 * Entity::getPosition(){
	return _vector3;
}
Vector3 * Entity::setPosition(double x, double y, double z){
	_vector3->set(x, y, z);
	return this->getPosition();
}

Vector3 * Entity::setPosition(const Vector3 &vec){
	_vector3->set(vec.getX(), vec.getY(), vec.getZ());
	return this->getPosition();
}

double Entity::getPosX(){
	return _vector3->getX();
}
double Entity::getPosY(){
	return _vector3->getY();
}
double Entity::getPosZ(){
	return _vector3->getZ();
}

Entity::~Entity()
{
}
