#include "Bus.h"
#include <GL/glut.h>

Bus::Bus(double x, double y, double z, double speed, bool direction) : DynamicObject(x, y, z, 2.16, 0.9, direction){ setSpeed(speed, 0, 0); }

Bus::~Bus()
{
}

void Bus::update(double dt){
	Vector3 s = _speed * (dt / 1000);
	setPosition(*getPosition() + s);
	if ((_speed.getX() > 0 && getPosX() > 16) || (getPosX() < -16))   restartMove();
}

void Bus::draw(){

	GLfloat amb[] = { 0.05f, 0.0f, 0.0f, 1.0f };
	GLfloat diff[] = { 0.5f, 0.4f, 0.4f, 1.0f };
	GLfloat spec[] = { 0.7f, 0.04f, 0.04f, 1.0f };
	GLfloat shine = 10.f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);

	//corpo
	glPushMatrix();
	glTranslatef(getPosX(), getPosY(), getPosZ() + 0.7); /*vao buscar estes valores ao gamemanager*/
	if (!getDirection())
		glRotatef(180, 0, 0, 1);
	glPushMatrix();
	glTranslatef(0.8, 0, 0); /*vao buscar estes valores ao gamemanager*/
	
	//ligacao ao meter aqui a ligacao ela nao se sobrepoem a caixa nem a cabeca
	glPushMatrix(); 
	glColor3f(0.5f, 0.5f, 0.9f);
	glTranslatef(-0.3f, 0.0f, 0.3f);
	glScalef(0.2, 0.45, 0.5);
	glutSolidCube(1);
	glPopMatrix();

	// roda esquerda da frente

	glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(-0.05f, 0.35f, -0.1f);
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glutSolidTorus(0.04, 0.1, 20, 20);
	glPopMatrix();

	// roda esquerda do meio

	glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(-0.6f, 0.35f, -0.1f);
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glutSolidTorus(0.04, 0.1, 20, 20);
	glPopMatrix();

	// roda esquerda de tr�s

	glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(-1.6f, 0.35f, -0.1f);
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glutSolidTorus(0.04, 0.1, 20, 20);
	glPopMatrix();

	// roda direita da frente

	glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(-0.05f, -0.35f, -0.1f);
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glutSolidTorus(0.04, 0.1, 20, 20);
	glPopMatrix();

	// roda direita do meio

	glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(-0.6f, -0.35f, -0.1f);
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glutSolidTorus(0.04, 0.1, 20, 20);
	glPopMatrix();


	// roda direita de tr�s

	glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(-1.6f, -0.35f, -0.1f);
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glutSolidTorus(0.04, 0.1, 20, 20);
	glPopMatrix();

	// Farol esquerdo
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 0.0f);
	glTranslatef(0.27f, 0.2f, 0.0f);
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glutSolidSphere(0.05, 10, 10);
	glPopMatrix();

	// Farol direito
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 0.0f);
	glTranslatef(0.27f, -0.2f, 0.0f);
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glutSolidSphere(0.05, 10, 10);
	glPopMatrix();

	//cabeca

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 0.25f);
	glColor3f(0.3f, 0.3f, 0.3f);
	glScalef(0.55, 0.7, 0.5);
	glutSolidCube(1);
	glPopMatrix();

	//caixa
	glPushMatrix();
	glColor3f(0.3f, 0.0f, 0.0f);
	glTranslatef(-1.1f, 0.0f, 0.3f);
	glScalef(1.5, 0.7, 0.5);
	glutSolidCube(1);
	glPopMatrix();

	glPopMatrix();
	glPopMatrix();
}