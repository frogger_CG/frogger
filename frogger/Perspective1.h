#ifndef __PERSPECTIVE1_H__
#define __PERSPECTIVE1_H__
#include "PerspectiveCamera.h"
class Perspective1 :
	public PerspectiveCamera
{
public:
	Perspective1(double fovy, double aspect, double near, double far) : PerspectiveCamera( fovy, aspect, near, far){
	}
	~Perspective1();
	void update(int width, int height);
	void computeProjectionMatrix();
	void computeVisualizationMatrix();
};

#endif
