#ifndef __LIGHTSOURCE_H__
#define __LIGHTSOURCE_H__


#include "Vector4.h"
#include "Vector3.h"
#include <Gl\glut.h>
class LightSource
{
private:
	Vector4* _ambient;
	Vector4* _diffuse;
	Vector4* _specular;
	Vector4* _position;
	Vector3* _direction;
	double _cut_off;
	double _exponent;
	GLenum _num;
	bool _state = true;

public:
	LightSource(GLenum num) {
		_num = num;
		_direction = new Vector3(0., 0., -1.);
		_position = new Vector4(0, -10, 10, 0);
		_ambient = new Vector4(0.1, 0.1, 0.1, 0);
		_diffuse = new Vector4(1, 1, 1, 1);
		_specular = new Vector4(0, 0, 0, 0);
		_exponent = 0.0;
		_cut_off = 180.0;
	}
	bool getState();
	bool setState(bool state);
	GLenum getNum();
	void setPosition(const Vector4 &position);
	void setDirection(const Vector3 &direction);
	void setCutOff(double cut_off);
	void setExponent(double exponent);
	void setAmbient(const Vector4 &ambient);
	void setDiffuse(const Vector4 &diffuse);
	void setSpecular(const Vector4 &specular);
	void draw();
	~LightSource(void);
};

#endif