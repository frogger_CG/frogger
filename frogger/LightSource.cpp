#include "LightSource.h"


void LightSource::setPosition(const Vector4 &position){
	_position->set(position.getX(), position.getY(), position.getZ(), position.getW());
}

void LightSource::setDirection(const Vector3 &direction){
	_direction->set(direction.getX(), direction.getY(), direction.getZ());
}

void LightSource::setCutOff(double cut_off){
	_cut_off = cut_off;
}

void LightSource::setExponent(double exponent){
	_exponent = exponent;
}

void LightSource::setAmbient(const Vector4 &ambient){
	_ambient->set(ambient.getX(), ambient.getY(), ambient.getZ(), ambient.getW());
}

void LightSource::setDiffuse(const Vector4 &diffuse){
	_diffuse->set(diffuse.getX(), diffuse.getY(), diffuse.getZ(), diffuse.getW());
}

void LightSource::setSpecular(const Vector4 &specular){
	_specular->set(specular.getX(), specular.getY(), specular.getZ(), specular.getW());
}

void LightSource::draw(){

	glClearColor(0.1f, 0.5f, .8f, 1.0f);
	float position[] = { _position->getX(), _position->getY(), _position->getZ(), _position->getW() };
	float ambient[] = { _ambient->getX(), _ambient->getY(), _ambient->getZ(), _ambient->getW() };
	float diffuse[] = { _diffuse->getX(), _diffuse->getY(), _diffuse->getZ(), _diffuse->getW() };
	float specular[] = { _specular->getX(), _specular->getY(), _specular->getZ(), _specular->getW() };
	float direction[] = { _direction->getX(), _direction->getY(), _direction->getZ() };

	glLightfv(_num, GL_SPOT_DIRECTION, direction);
	glLightfv(_num, GL_POSITION, position);
	glLightfv(_num, GL_AMBIENT, ambient);
	glLightfv(_num, GL_DIFFUSE, diffuse);
	glLightfv(_num, GL_SPECULAR, specular);
	glLightf(_num, GL_SPOT_CUTOFF, _cut_off);
	glLightf(_num, GL_SPOT_EXPONENT, _exponent);
	
}









LightSource::~LightSource()
{
}
