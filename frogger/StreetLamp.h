#ifndef __STREETLAMP_H__
#define __STREETLAMP_H__

#include "StaticObject.h"
class StreetLamp :
	public StaticObject
{
public:
	StreetLamp(double x, double y, double z);
	~StreetLamp();
	void draw();
};

#endif