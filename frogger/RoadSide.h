#ifndef __ROADSIDE_H__
#define __ROADSIDE_H__

#include "StaticObject.h"
class RoadSide :
	public StaticObject
{
public:
	RoadSide(double x, double y, double z);
	~RoadSide();
	void draw();
};

#endif

