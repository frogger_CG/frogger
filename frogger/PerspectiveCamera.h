#ifndef __PERSPECTIVECAMERA_H__
#define __PERSPECTIVECAMERA_H__

#include "Camera.h"

class PerspectiveCamera :
	public Camera
{
protected:
	double _fovy;
	double _aspect;
public:
	PerspectiveCamera(double fovy, double aspect, double near, double far) : Camera(near, far){
		_fovy = fovy;
		_aspect = aspect;
	}
	~PerspectiveCamera();
	virtual void update(int width, int height) = 0;
	virtual void computeProjectionMatrix()=0;
	virtual void computeVisualizationMatrix()=0;
};

#endif
