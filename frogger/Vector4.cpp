#include "Vector4.h"


double Vector4::getW() const{
	return _w;
}

void Vector4::set(double x, double y, double z, double w){
	_x = x;
	_y = y;
	_z = z;
	_w = w;
}

Vector4 Vector4::operator* (double vec){
	return Vector4(_x * vec, _y * vec, _z * vec, _w * vec);
}

Vector4 Vector4::operator+ (const Vector4& vec){
	return Vector4(_x + vec.getX(), _y + vec.getY(), _z + vec.getZ(), _w + vec.getW());
}

Vector4 Vector4::operator- (const Vector4& vec){
	return Vector4(_x - vec.getX(), _y - vec.getY(), _z - vec.getZ(), _w - vec.getZ());
}

Vector4::~Vector4(void)
{
}