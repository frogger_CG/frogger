#include "GameObject.h"
#include <iostream>

Vector3 GameObject::getSize(){
	return _size;
}

void GameObject::setDirection(bool direction){
	_direction = direction;
}

bool GameObject::getDirection(){
	return _direction;
}

void GameObject::setNewX(int value){
	int pos = 16;
	GameObject * go = reinterpret_cast<GameObject *> (value);
	if (go->getDirection())
		pos = -16;
	go->setPosition(pos, go->getPosY(), go->getPosZ());
}

void GameObject::restartMove(){
	/* initialize random seed: */
	srand(time(NULL));

	/* generate secret number between 1 and 100: */
	glutTimerFunc(rand() % 100 + 1, setNewX, reinterpret_cast<long int> (this));
}

void GameObject::LoadTexture(){
	if (_texture == 0)
		_texture = SOIL_load_OGL_texture(_name, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
		//std::cout << _texture[0];
	glBindTexture(GL_TEXTURE_2D, _texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

}