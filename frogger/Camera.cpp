#include "Camera.h"
#include "GL\glut.h"


Camera::~Camera()
{
}

void Camera::setPositionCamera(float camPosX, float camPosY){
	_camPosX = camPosX;
	_camPosY = camPosY;
}

void Camera::setDirectionVar(int dir){
	_direction = dir;
}

void Camera::setDirection(int dir){
	if (dir == UP){
		_camX = 0;
		_camY = -3;
	}
	else if (dir == DOWN){
		_camX = 0;
		_camY = 3;
	}
	else if (dir == LEFT){
		_camX = 3;
		_camY = 0;
	}
	else if (dir == RIGHT){
		_camX = -3;
		_camY = 0;
	}
}