#include "collisionDetetor.h"
#include "GameManager.h"


collisionDetetor::collisionDetetor(GameManager * game) : _game(game), _frog(_game->getFrog())
{
}


collisionDetetor::~collisionDetetor()
{
}

void collisionDetetor::detectColisions(){
	_frog->setTimberSpeed(Vector3(0, 0, 0));
	if (_frog->edgeOfBoard())
		_frog->setWalk(false);
	else
		_frog->setWalk(true);
	std::vector<GameObject *> gameObjects = _game->getGameObjects();
	for (unsigned int i = 0; i < gameObjects.size() - 1; i++) {
		if (gameObjects[i] != _frog && detectColision(_frog, gameObjects[i])){
			if (_frog->getPosY() < 0) {
				_game->looselife();
				_frog->setPosition(0, -7.43, 0);
			}
			else if (_frog->getPosY() > 0) {
				_frog->setTimberSpeed(dynamic_cast<DynamicObject*>(gameObjects[i])->getSpeed());
			}
			return;
		}
	}
	if (_frog->getPosY() > 0.6 && _frog->getPosY() < 6.4){
		_frog->setPosition(0, -7.43, 0);
		_game->looselife();
	}
}

bool intersects(Vector3 leftUp1, Vector3 rightDown1, Vector3 leftUp2, Vector3 rightDown2){
	return (leftUp1.getX() < (rightDown2.getX()) && (rightDown1.getX() > leftUp2.getX())
		&& (leftUp1.getY() < rightDown2.getY()) && rightDown1.getY() > leftUp2.getY());
}

bool collisionDetetor::detectColision(Frog * frog, GameObject * go){
	if (go->isCollidible()){
		Vector3 leftUp1 = *frog->getPosition() - (frog->getSize()*0.5);
		Vector3 rightDown1 = *frog->getPosition() + (frog->getSize()*0.5);
		Vector3 leftUp2 = *go->getPosition() - (go->getSize()*0.5);
		Vector3 rightDown2 = *go->getPosition() + (go->getSize()*0.5);
		return intersects(leftUp1, rightDown1, leftUp2, rightDown2);
	}
	else
		return false;
}
