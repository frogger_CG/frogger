#ifndef __VECTOR3_H__
#define __VECTOR3_H__
class Vector3
{
protected:
	double _x;
	double _y;
	double _z;
public:
	Vector3(double x, double y, double z) : _x(x), _y(y), _z(z){}
	Vector3() :_x(0), _y(0), _z(0){}
	~Vector3(){}
	double getX() const;
	double getY() const;
	double getZ() const;
	void set(double x, double y, double z);
	bool isNull();
	Vector3 operator= (const Vector3& vec);
	Vector3 operator+ (const Vector3 &vec);
	Vector3 operator- (const Vector3& vec);
	Vector3 operator* (double x);
};

#endif
