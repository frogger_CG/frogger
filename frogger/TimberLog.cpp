#include "TimberLog.h"
#include <GL/glut.h>

TimberLog::TimberLog(double x, double y, double z, double speed, bool dir) : DynamicObject(x, y, z, 1.5, 0.7, dir){ setSpeed(speed, 0, 0); }

TimberLog::~TimberLog()
{
}

void TimberLog::update(double dt){
	Vector3 s = _speed * (dt / 1000);
	setPosition(*getPosition() + s);
		if ((_speed.getX() > 0 && getPosX() > 16) || (getPosX() < -16))   restartMove();
}

void TimberLog::draw(){

	

	GLfloat amb[] = { 0.15f, 0.1f, 0.05f, 1.0f };
	GLfloat diff[] = { 0.5f, 0.35f, 0.05f, 1.0f };
	GLfloat spec[] = { 0.5f, 0.35f, 0.05f, 1.0f };
	GLfloat shine = 44.8f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);


	glPushMatrix();
	glTranslatef(getPosX(), getPosY(), getPosZ() + 0.2);

	glPushMatrix();
	glColor3f(0.6f, 0.35f, 0.15f);
	glScalef(2.5, 0.9, 0.5);
	glutSolidCube(1);
	glPopMatrix();

	glPopMatrix();
}