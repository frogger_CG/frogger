#ifndef __PERSPECTIVE2_H__
#define __PERSPECTIVE2_H__



#include "PerspectiveCamera.h"
class Perspective2 :
	public PerspectiveCamera
{	
public:
	Perspective2(double fovy, double aspect, double near, double far) : PerspectiveCamera(fovy, aspect, near, far){
	}
	~Perspective2();
	void update(int width, int height);
	void computeProjectionMatrix();
	void computeVisualizationMatrix();
};

#endif
