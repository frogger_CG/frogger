#include "Car.h"
#include <GL/glut.h>


Car::Car(double x, double y, double z, double speed, bool dir) : DynamicObject(x, y, z, 1.55, 0.7, dir){ setSpeed(speed, 0, 0); }

Car::~Car()
{
}

void Car::update(double dt){
	Vector3 s = _speed * (dt / 1000);
	setPosition(*getPosition() + s);
	if ((_speed.getX() > 0 && getPosX() > 16) || (getPosX() < -16))   restartMove();
}

void Car::drawSquare() {
	glBegin(GL_TRIANGLES);

	glVertex3f(-.25, -.5, .125);//superior
	glVertex3f(0., -.5, .5);
	glVertex3f(-.5, -.5, .5);
	glVertex3f(-.25, -.5, .125);//direito
	glVertex3f(0., -.5, -.25);
	glVertex3f(0., -.5, .5);
	glVertex3f(-.25, -.5, .125);//inferior
	glVertex3f(-0.5, -.5, -.25);
	glVertex3f(0., -.5, -.25);
	glVertex3f(-.25, -.5, .125);//superior
	glVertex3f(-0.5, -.5, .5);
	glVertex3f(-0.5, -.5, -.25);
	glEnd();

}

void Car::drawWheel() {
	glBegin(GL_QUAD_STRIP);
	glNormal3f(0., 0., 1.);
	glVertex3f(-.15, 0., .6);
	glVertex3f(-.15, 0., .5);

	glNormal3f(1., 0., 1.);
	glVertex3f(-.106, .106, .6);
	glVertex3f(-.106, .106, .5);
	
	glNormal3f(0., 1., 0.);
	glVertex3f(0, 0.15, .6);
	glVertex3f(0, 0.15, .5);
	
	glNormal3f(1., -1., 0.);
	glVertex3f(0.106, 0.106, .6);
	glVertex3f(0.106, 0.106, .5);

	glNormal3f(0., 0., -1.);
	glVertex3f(.15, 0., .6);
	glVertex3f(.15, 0., .5);

	glNormal3f(-1., 0., -1.);
	glVertex3f(.106, -0.106, .6);
	glVertex3f(.106, -0.106, .5);

	glNormal3f(0., -1., 0.);
	glVertex3f(0, -0.15, 0.6);
	glVertex3f(0, -0.15, 0.5);

	glNormal3f(0., -1., 1.);
	glVertex3f(-0.106, -0.106, 0.6);
	glVertex3f(-0.106, -0.106, 0.5);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-.15, 0., .6);
	glVertex3f(-.106, .106, .6);
	glVertex3f(0, 0.15, .6);
	glVertex3f(0.106, 0.106, .6);
	glVertex3f(.15, 0., .6);
	glVertex3f(.106, -0.106, .6);
	glVertex3f(0, -0.15, 0.6);
	glVertex3f(-0.106, -0.106, 0.6);
	glEnd();

	
}


	void Car::draw(){

		//corpo
		glPushMatrix();
		glTranslatef(getPosX(), getPosY(), getPosZ() + 0.9);

		if (!getDirection())
			glRotatef(180, 0, 0, 1);

		//corpo

		GLfloat amb[] = { 0.02f, 0.07f, 0.1f, 1.0f };
		GLfloat diff[] = { 0.0f, 0.0f, 0.51f, 1.0f };
		GLfloat spec[] = { 0.0f, 0.0f, 0.37f, 1.0f };
		GLfloat shine = 76.8f;
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);

		glPushMatrix();
		glColor3f(0.0f, 0.0f, 1.0f);
		glScalef(1.5, 0.5, 0.5);
		
		glPushMatrix();
		glNormal3f(0., -1., 0.); //esquerda
		drawSquare(); 
		glTranslatef(0.5f, 0.0f, 0.0f);
		drawSquare();
		glPopMatrix();

		glPushMatrix();
		glNormal3f(0., 1., 0.); //direita
		glTranslatef(0.f, 1.f, 0.0f);
		drawSquare();
		glTranslatef(0.5f, 0.0f, 0.0f);
		drawSquare();
		glPopMatrix();

		glPushMatrix();
		glNormal3f(1., 0., 0.); //frente
		glTranslatef(0., 0.5, 0.);
		glScalef(1., 2., 1.);
		glRotatef(90, 0., 0., 1.);
		drawSquare();
		glPopMatrix();

		glPushMatrix();
		glNormal3f(-1., 0., 0.); //tras
		glTranslatef(-1., 0.5, 0.);
		glScalef(1., 2., 1.);
		glRotatef(90, 0., 0., 1.);
		drawSquare();
		glPopMatrix();

		glPushMatrix();
		glNormal3f(0., 0., 1.); //tras
		glTranslatef(0.5, 0.2, 0.5);
		glScalef(2., 1.5, 1.);
		glRotatef(90, 1., 0., 0.);
		drawSquare();
		glPopMatrix();


		//glNormal3f(-1., 0., 0.); //frente
		//glTranslatef(1.,0., 0.);
		//glScalef(1., 2., 1.);
	
	
		//glutSolidCube(1);
		glPopMatrix();

		//tecto
		
		/*
		glPushMatrix();
		glColor3f(0.0f, 0.5f, 0.0f);
		glTranslatef(0.0f, 0.0f, 0.3f);
		glScalef(0.5, 0.5, 0.5);

		//glutSolidCube(1);
		glPopMatrix();
		*/

		// Vidro
		glPushMatrix();
		glBegin(GL_QUADS);
		glNormal3f(1., 0., 1.);
		glVertex3f(0.2, -0.25, 0.1);
		glVertex3f(0., -0.25, .5);
		glVertex3f(0., 0.25, .5);
		glVertex3f(0.2, 0.25, 0.1);
		glEnd();
		glPopMatrix();


		
		// roda esquerda da frente
		GLfloat amb1[] = { 0.1f, 0.08f, 0.1f, 1.0f };
		GLfloat diff1[] = { 0.22f, 0.23f, 0.26f, 1.0f };
		GLfloat spec1[] = { 0.26f, 0.26f, 0.29f, 1.0f };
		GLfloat shine1 = 76.8f;
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb1);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff1);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec1);
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine1);


		glPushMatrix();
		glColor3f(0.0f, 1.0f, 1.0f);
		glTranslatef(0.6f, -.3f, -0.1f);
		glRotatef(-90, 1.0f, 0.0f, 0.0f);
		//glutSolidTorus(0.05, 0.1, 20, 20);
		drawWheel();
		/*
		glBegin(GL_QUADS);
		glVertex3f(-.15,0.,.7);
		glVertex3f(0, 0.15, .7);
		glVertex3f(.15,0.,.7);
		glVertex3f(0,-0.15,0.7);
		glEnd();


		glBegin(GL_QUADS);
		glVertex3f(-.106, .106, .7);
		glVertex3f(0.106, 0.106, .7);
		glVertex3f(.106, -0.106, .7);
		glVertex3f(-0.106, -0.106, 0.7);
		glEnd();
		*/
		//glBegin(GL_QUAD_STRIP);
		//glNormal3f();
		


		glPopMatrix();

		// roda esquerda de tr�s

		glPushMatrix();
		glColor3f(0.0f, 1.0f, 1.0f);
		glTranslatef(-0.6f, -.3f, -0.1f);
		glRotatef(-90, 1.0f, 0.0f, 0.0f);
		drawWheel();
		//glutSolidTorus(0.05, 0.1, 20, 20);
		glPopMatrix();

		// roda direita da frente

		glPushMatrix();
		glColor3f(0.0f, 1.0f, 1.0f);
		glTranslatef(0.6f, 0.3f, -0.1f);
		glRotatef(90, 1.0f, 0.0f, 0.0f);
		drawWheel();
		//glutSolidTorus(0.05, 0.1, 20, 20);
		glPopMatrix();

		// roda direita de tr�s

		glPushMatrix();
		glColor3f(0.0f, 1.0f, 1.0f);
		glTranslatef(-0.6f, 0.3f, -0.1f);
		glRotatef(90, 1.0f, 0.0f, 0.0f);
		drawWheel();
		//glutSolidTorus(0.05, 0.1, 20, 20);
		glPopMatrix();

		// Farol esquerdo
		glPushMatrix();
		glColor3f(1.0f, 1.0f, 0.0f);
		glTranslatef(0.75f, 0.2f, 0.0f);
		glRotatef(90, 1.0f, 0.0f, 0.0f);
		glutSolidSphere(0.05, 10, 10);
		glPopMatrix();


		// Farol direito
		glPushMatrix();
		glColor3f(1.0f, 1.0f, 0.0f);
		glTranslatef(0.75f, -0.2f, 0.0f);
		glRotatef(90, 1.0f, 0.0f, 0.0f);
		glutSolidSphere(0.05, 10, 10);
		glPopMatrix();


		glPopMatrix();
	}