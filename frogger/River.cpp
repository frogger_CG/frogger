#include "River.h"
#include <GL/glut.h>
#include <GL/SOIL.h>

River::River(double x, double y, double z) : StaticObject(x, y, z)
{
	_name = "river.png";
	 
}

void River::draw(){
	
	/*	glTranslatef(getPosX(), getPosY(), getPosZ() - 0.2);
		glColor4f(0.0f, 0.8f, 0.9f, 0);
		glScalef(30, 5.8, 1);
		glutSolidCube(1);*/
	
	GLfloat amb[] = { 0.1f, 0.08f, 0.1f, 1.0f };
	GLfloat diff[] = { 0.10f, 0.10f, 0.7f, 1.0f };
	GLfloat spec[] = { 0.10f, 0.10f, 0.7f, 1.0f };
	GLfloat shine = 76.8f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);

	glPushMatrix();
	glColor4f(0.0f, 0.8f, 0.9f, 0);
	glEnable(GL_TEXTURE_2D);
	LoadTexture();
	desenhaMalha(-15, 15, 0, 6.1, 0.3);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}


