#include "Frog.h"
#include <GL/glut.h>

Frog::Frog(double x, double y, double z) : 
	DynamicObject(x, y, z, 1.5, 1.2, true), _up(false), _down(false), _left(false), _right(false), _canWalk(true){
	_timberSpeed = Vector3(0, 0, 0);
}

bool Frog::isMovingSet(){
	return (_up || _down || _left || _right);
}

void Frog::update(double dt){
	if (_canWalk){
		Vector3 s = (_speed + _timberSpeed) * (dt / 1000);
		setPosition(*getPosition() + s);
	}
}

void Frog::setWalk(bool b){
	_canWalk = b;
}

void Frog::setTimberSpeed(Vector3 speed){
	_timberSpeed = speed;
}

void Frog::setDirection(int dir){
	_up = _down = _left = _right = false;
	if (dir == UP){
		_up = true;
		setSpeed(0,4,0);
	}
	else if (dir == DOWN){
		_down = true;
		setSpeed(0, -4, 0);
	}
	else if (dir == LEFT){
		_left = true;
		setSpeed(-4, 0, 0);
	}
	else if (dir == RIGHT){
		_right = true;
		setSpeed(4, 0, 0);
	}
}

void Frog::stop(){
	_speed = Vector3(0, 0, 0);
}

bool Frog::edgeOfBoard(){
	return (_down && getPosY() < -7.2) || (_up && getPosY() > 7.2) || (!_timberSpeed.isNull() && _left && getPosX() < -12.2) || 
		(!_timberSpeed.isNull() && _right && getPosX() > 12.2) || (getPosX() < -14 && _left) || (getPosX() > 14 && _right);
}

void Frog::draw(){
	glPushMatrix();

	glTranslatef(getPosX(), getPosY(), getPosZ() + 0.7);
	glRotatef(90., 0., 0., 1.);
	
	glScalef(0.5, 0.5, 0.5);
	
	if (_up){
		glRotatef(0., 0., 0., 1.);
	}
	else if (_down){
		glRotatef(180., 0., 0., 1.);
	}
	else if (_left){
		glRotatef(90., 0., 0., 1.);
	}
	else if (_right){
		glRotatef(-90., 0., 0., 1.);
	}

	frogbody();
	glPopMatrix();
}

void Frog::frogbody(){

	glPushMatrix();
	GLfloat amb[] = { 0.11f, 0.06f, 0.1f, 1.0f };
	GLfloat diff[] = { 0.0f, 0.44f, 0.0f, 1.0f };
	GLfloat spec[] = { 0.64f, 0.63f, 0.64f, 1.0f };
	GLfloat shine = 76.8f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);


	//corpo
	glPushMatrix();
	glTranslatef(-0.2, 0, 0);

	//corpo

	glPushMatrix();
	glRotatef(-30, 1, 1, 0);
	glScalef(2.0, 1.5, 1.0);
	glColor3f(0.0f, 0.8f, 0.0f);
	glutSolidSphere(0.5, 10, 10);
	glPopMatrix();

	// perna de tras direita

	glPushMatrix();
	glTranslatef(-0.5, 0.6, -0.4);
	glScalef(0.4, 0.8, 0.1);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidCube(0.6);
	glPopMatrix();

	// perna da frente direita (1 de 2)

	glPushMatrix();
	glTranslatef(0.5, 0.8, 0);
	glScalef(0.1, 1.2, 0.2);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidCube(0.6);
	glPopMatrix();

	// perna da frente direita (2 de 2)
	glPushMatrix();
	glTranslatef(0.68, 1.2, 0);
	glScalef(0.8, 0.1, 0.2);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidCube(0.6);
	glPopMatrix();


	// perna de tras esquerda

	glPushMatrix();
	glTranslatef(-0.5, -0.6, -0.4);
	glScalef(0.4, 0.8, 0.1);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidCube(0.6);
	glPopMatrix();

	// perna da frente esquerda (1 de 2)

	glPushMatrix();
	glTranslatef(0.5, -0.8, 0);
	glScalef(0.1, 1.2, 0.2);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidCube(0.6);
	glPopMatrix();

	// perna da frente esquerda (2 de 2)
	glPushMatrix();
	glTranslatef(0.68, -1.2, 0);
	glScalef(0.8, 0.1, 0.2);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidCube(0.6);
	glPopMatrix();

	// pata esquerda da frente
	glPushMatrix();
	glTranslatef(0.9, -1.2, 0);
	glScalef(0.2, 0.25, 0.08);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidSphere(0.6, 10, 10);
	glPopMatrix();

	// pata esquerda de tr�s
	glPushMatrix();
	glTranslatef(-0.3, -1, -0.4);
	glScalef(0.65, 0.4, 0.05);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidSphere(0.6, 10, 10);
	glPopMatrix();

	// pata direita da frente
	glPushMatrix();
	glTranslatef(0.9, 1.2, 0);
	glScalef(0.2, 0.25, 0.08);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidSphere(0.6, 10, 10);
	glPopMatrix();

	// pata direita de tr�s
	glPushMatrix();
	glTranslatef(-0.3, 0.9, -0.4);
	glScalef(0.65, 0.4, 0.05);
	glColor3f(0.0f, 0.5f, 0.0f);
	glutSolidSphere(0.6, 10, 10);
	glPopMatrix();

	//Cabe�a

	glPushMatrix();
	glTranslatef(1, 0, 0.5);
	glScalef(1, 1.5, 1);
	glColor3f(0.0f, 0.2f, 0.0f);
	glutSolidSphere(0.5, 10, 10);
	glPopMatrix();


	// olho direito(fora)
	glPushMatrix();
	glTranslatef(1.1, 0.2, 1);
	glColor3f(0.0f, 0.3f, 0.0f);
	glutSolidSphere(0.1, 10, 10);
	glPopMatrix();

	// olho esquerdo(fora)
	glPushMatrix();
	glTranslatef(1.1, -0.2, 1);
	glColor3f(0.0f, 0.3f, 0.0f);
	glutSolidSphere(0.1, 10, 10);
	glPopMatrix();

	glPopMatrix();

	glPushMatrix();
	GLfloat amb1[] = { 0.05f, 0.05f, 0.05f, 1.0f };
	GLfloat diff1[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	GLfloat spec1[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat shine1 = 10.f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb1);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff1);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec1);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine1);

	// olho direito(dentro)
	glPushMatrix();
	glTranslatef(1.13, 0.2, 1);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidSphere(0.08, 10, 10);
	glPopMatrix();


	// olho esquerdo(dentro)
	glPushMatrix();
	glTranslatef(1.13, -0.2, 1);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidSphere(0.08, 10, 10);
	glPopMatrix();

	glPopMatrix();

	glPushMatrix();
	GLfloat amb2[] = { 0.05f, 0.0f, 0.0f, 1.0f };
	GLfloat diff2[] = { 0.5f, 0.4f, 0.4f, 1.0f };
	GLfloat spec2[] = { 0.7f, 0.04f, 0.04f, 1.0f };
	GLfloat shine2 = 10.f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb2);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff2);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec2);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine2);


	// boca do sapo
	glPushMatrix();
	glTranslatef(1.4, 0, 0.5);
	glScalef(1.2, 5, 1);
	glColor3f(1.0f, 0.0f, 0.0f);
	glutSolidSphere(0.1, 10, 10);
	glPopMatrix();


	glPopMatrix();
	glPopMatrix();

	//glFlush();
}