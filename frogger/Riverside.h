#ifndef __RIVERSIDE_H__
#define __RIVERSIDE_H__

#include "StaticObject.h"
class Riverside :
	public StaticObject
{
public:
	Riverside(double x, double y, double z);
	~Riverside();
	void draw();
};

#endif



