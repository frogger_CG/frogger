#include "Road.h"
#include <GL/glut.h>

void Road::draw(){
	/*
	glPushMatrix();
	glTranslatef(getPosX(), getPosY(), getPosZ()); 
		glColor3f(0.2f, 0.2f, 0.2f);
		glScalef(30, 5.8, 1);
		glutSolidCube(1);
	glPopMatrix();
	*/

	GLfloat amb[] = { 0.05f, 0.05f, 0.04f, 1.0f };
	GLfloat diff[] = { 0.2f, 0.18f, 0.25f, 1.0f };
	GLfloat spec[] = { 0.13f, 0.09f, 0.13f, 1.0f };
	GLfloat shine = 83.0f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);


	glPushMatrix();
	glColor3f(0.2f, 0.2f, 0.2f);
	glEnable(GL_TEXTURE_2D);
	LoadTexture();
	desenhaMalha(-15, 15, -6.6, -0.6 , 0.5);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

Road::Road(double x, double y, double z) : StaticObject( x, y, z)
{
	_name = "road.jpg";
}


Road::~Road()
{
}
