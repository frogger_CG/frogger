#include "GameObject.h"
#include "GameManager.h"
#include <GL\glut.h>
#include <GL\SOIL.h>
static GameManager * game;
static GLuint texture;

void myReshape(GLsizei width, GLsizei height){
	//glMatrixMode(GL_TEXTURE);
	game->reshape(width, height);
}

void myDisplay(void) {
	//glMatrixMode(GL_TEXTURE);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	game->display();		
	glutSwapBuffers();
	//glFlush();
}

void moveAction(int t){
	game->onTimer();
	glutTimerFunc(10, moveAction, 0);
}

void keyPress(unsigned char c, int i, int j){
	game->keyPressed(c, i, j);
}

void keyboardUp(unsigned char c, int i, int j){
	game->keyboardUp(c, i, j);
}

int main(int argc, char * argv[]) {
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE| GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(1040, 640);
	glutInitWindowPosition(-1, -1);
	
	game = new GameManager();

	glutCreateWindow("Frogger");
	glutDisplayFunc(myDisplay); //mydisplay serve para inicializar os objectos
	glutReshapeFunc(myReshape); //o reshape e so para ajustar a janela
	glutTimerFunc(30, moveAction, 30); /*de 30 em 30 ms e chamado o moveaction*/
	glutKeyboardFunc(keyPress); /*quando premir a tecla*/
	glutKeyboardUpFunc(keyboardUp); /*quando a tecla e largada*/
	glEnable(GL_NORMALIZE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);


	glutMainLoop(); /*esta e a rotina que processa os eventos*/

	return 0;
}
