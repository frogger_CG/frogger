#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__


#include "GameObject.h"
#include "Frog.h"
#include "River.h"
#include "Riverside.h"
#include "Road.h"
#include "RoadSide.h"
#include "Menu.h"
#include "Car.h"
#include "TimberLog.h"
#include "Bus.h"
#include "Camera.h"
#include "OrthogonalCamera.h"
#include "PerspectiveCamera.h"
#include "Perspective1.h"
#include "Perspective2.h"
#include "LightSource.h"
#include "StreetLamp.h"
#include <vector>
#include <GL\glut.h>
class collisionDetetor;
class GameManager
{
private:
	std::vector<GameObject *> _game_objects;
	std::vector<GameObject *> _game_objects_lifes;
	std::vector<GameObject *> _game_objects_menu;
	std::vector<LightSource *> _light_sources;
	Frog *_frog;
	collisionDetetor * _collisionDetetor;
	long unsigned int _time, _initTime;
	int _keyPressed = 0;
	Camera * _camera;
	Camera * _camera1 = new OrthogonalCamera(-15, 15, 8, -8, -20, 20);
	int _width;
	int _height;
	bool camera3 = false;
	bool _headlight = false;
	int _timerOut=0;
	bool _pause = false;
	int _lifes = 5;
public:
	GameManager();
	~GameManager();
	void display();
	void keyPressed(unsigned char c, int i, int j);
	void keyboardUp(unsigned char c, int i, int j);
	void onTimer();
	void idle();
	void update();
	void updateGameSpeed();
	void init();
	void reshape(GLsizei width, GLsizei height);
	void addGameObject(GameObject * go);
	void removeGameObject(GameObject * go);
	std::vector<GameObject *> getGameObjects();
	Frog * getFrog();
	void looselife();
	void addGameObjectLife(GameObject * go);
	void addGameObjectMenu(GameObject * go);
};

#endif