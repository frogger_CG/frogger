#ifndef __COLLISIONDETETOR_H__
#define __COLLISIONDETETOR_H__

#include "Frog.h"

class GameManager;

class collisionDetetor
{
private:
	GameManager * _game;
	Frog * _frog;
public:
	collisionDetetor(GameManager * game);
	~collisionDetetor();
	void detectColisions();
	bool detectColision(Frog * frog, GameObject * go);
};


#endif