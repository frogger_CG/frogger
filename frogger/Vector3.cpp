#include "Vector3.h"

double Vector3::getX() const{
	return _x;
}

double Vector3::getY() const{
	return _y;
}

double Vector3::getZ() const{
	return _z;
}

void Vector3::set(double x, double y, double z){
	_x = x;
	_y = y;
	_z = z;
}

bool Vector3::isNull(){
	return _x == _y == _z == 0;
}


Vector3 Vector3::operator* (double vec){
	return Vector3(_x * vec, _y * vec, _z * vec);
}

Vector3 Vector3::operator+ (const Vector3& vec){
	return Vector3(_x + vec.getX(), _y + vec.getY(), _z + vec.getZ());
}

Vector3 Vector3::operator- (const Vector3& vec){
	return Vector3(_x - vec.getX(), _y - vec.getY(), _z - vec.getZ());
}

Vector3 Vector3::operator= (const Vector3& vec){
	set(vec.getX(), vec.getY(), vec.getZ());
	return Vector3(_x, _y, _z);
}

