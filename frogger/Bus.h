#ifndef __BUS_H__
#define __BUS_H__

#include "DynamicObject.h"
class Bus :
	public DynamicObject
{
public:
	Bus(double x, double y, double z, double speed, bool direction);
	void draw();
	void setDirection(int dir);
	void stop();
	void update(double dt);
	bool isMovingSet();
	~Bus();
};
#endif