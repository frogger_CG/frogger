#ifndef __RIVER_H__
#define __RIVER_H__

#include "StaticObject.h"
class River :
	public StaticObject
{
public:
	River(double x, double y, double z);
	~River(){}
	void draw();
};

#endif

