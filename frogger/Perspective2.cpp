#include "Perspective2.h"
#include "GL\glut.h"




Perspective2::~Perspective2()
{
}




void Perspective2::update(int w, int h){
	float ratio = _aspect;
	float aspect = _fovy;
	if (aspect > ratio)
		glViewport((w - h*ratio) / 2, 0, h*ratio, h);
	else
		glViewport(0, (h - w / ratio) / 2, w, w / ratio);
	computeProjectionMatrix();
	computeVisualizationMatrix();
}

void Perspective2::computeProjectionMatrix(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(_fovy, _aspect, 1, 50);
}

void Perspective2::computeVisualizationMatrix(){
	int camX = 0, camY = -5;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	setDirection(_direction);
	gluLookAt(_camPosX + _camX, _camPosY  + _camY , 2.5, //Camera Position
		_camPosX, _camPosY, 0.0,		//Looking at
		0.0, 0.0, 1.0);		//Up Vector

}

