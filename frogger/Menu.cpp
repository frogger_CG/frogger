#include "Menu.h"


Menu::Menu(double x, double y, double z, const char* menuName) : StaticObject(x, y, z)
{
	_name = menuName;
}


Menu::~Menu()
{
}
void Menu::draw(){
	GLfloat amb[] = { 1.f, 1.f, 1.f, 1.0f };
	GLfloat diff[] = { 1.f, 1.f, 1.f, 1.0f };
	GLfloat spec[] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat shine = 100.0f;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shine);

	glPushMatrix();
	glColor4f(1.0f, 1.0f, 1.0f, 0);
	glEnable(GL_TEXTURE_2D);
	LoadTexture();

	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-5.0f, 5.0f, 10.0f);
	//top left
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-5.0f, -5.0f, 10.0f);
	//bottom left
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(5.0f, -5.0f, 10.0f);	//bottom right
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(5.0f, 5.0f, 10.0f);	//top right
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}